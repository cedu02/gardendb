using System;
using System.Runtime.Serialization;
using FluentAssertions;
using GardenCore.Models;
using GardenDB.Cache.Helper;
using Xunit;

namespace GardenDBTest {
    public class ByteArrayHelperTest {
        [Fact]
        public void ObjectToByteArray_RandomSerializableObject_ReturnsByteArray() {
            // Arrange 
            var testObject = new TaskResult();
            // Act
            var actual = ByteArrayHelper.ObjectToByteArray(testObject);
            // Assert
            Assert.True(actual.Length > 0);
        }

        [Fact]
        public void ObjectToByteArray_NotSerializableObject_ThrowsSerializableException() {
            // Arrange 
            var testObject = new NotSerializable();
            // Act
            Action act = () => ByteArrayHelper.ObjectToByteArray(testObject);
            // Assert 
            act.Should().Throw<SerializationException>();
        }


        [Fact]
        public void ObjectToByteArray_Null_ReturnsEmptyArray() {
            // Arrange and Act
            var actual = ByteArrayHelper.ObjectToByteArray(null);
            // Assert 
            actual.Should().BeEquivalentTo(new byte[0]);
        }

        [Fact]
        public void FromByteArray_DeserializeValidObject_ReturnsObject() {
            // Arrange
            var expectedObject = new TaskResult {
                Success = true,
                Message = "Hallo"
            };
            var testByte = ByteArrayHelper.ObjectToByteArray(expectedObject);
            // Act
            var actual = ByteArrayHelper.FromByteArray<TaskResult>(testByte);
            // Assert 
            actual.Should().BeEquivalentTo(expectedObject);
        }


        [Fact]
        public void FromByteArray_DeserializeInvalidObject_ThrowsException() {
            // Arrange
            var expectedObject = new TaskResult {
                Success = true,
                Message = "Hallo"
            };

            var testByte = ByteArrayHelper.ObjectToByteArray(expectedObject);
            // destroy byte array
            for (var i = 0; i < testByte.Length; i++)
                if (testByte[i] == 0)
                    testByte[i] = 1;

            // Act
            Action act = () => ByteArrayHelper.FromByteArray<TaskResult>(testByte);
            // Assert 
            act.Should().Throw<SerializationException>();
        }

        [Fact]
        public void FromByteArray_DeserializeNull_ShouldReturnEmptyT() {
            // Arrange and Act
            var actual = ByteArrayHelper.FromByteArray<TaskResult>(null);
            // Assert 
            actual.Should().BeEquivalentTo(new TaskResult());
        }


        private class NotSerializable {
            public string Test { get; set; }
        }
    }
}