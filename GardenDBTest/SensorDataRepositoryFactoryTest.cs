using System;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenDB.Database.Repositories;
using GardenDB.Interfaces;
using GardenDBTest.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace GardenDBTest {
    public class SensorDataRepositoryFactoryTest {
        [Fact]
        public void CreateRepository_Valid_ReturnsRepositoryInstance() {
            // Arrange 
            var services = new ServiceCollection();
            services.AddTransient<IGasSensorRepository, GasSensorRepository>();
            services.AddTransient<IDbService, TestDbService>();
            services.AddTransient<ILogger<SensorRepository>, NullLogger<SensorRepository>>();
            services.AddTransient<ILogger<GasSensorRepository>, NullLogger<GasSensorRepository>>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            var serviceProvider = services.BuildServiceProvider();
            var sut = CreateSut(serviceProvider);

            // Act
            var actual = sut.CreateRepository(SensorEnum.GasSensor)!;
            // Assert
            SensorEnum.GasSensor.Repository.IsInstanceOfType(actual).Should().BeTrue();
        }

        [Fact]
        public void CreateRepository_ValidPressureRepository_ReturnsRepositoryInstance() {
            // Arrange 
            var services = new ServiceCollection();
            services.AddTransient<IPressureRepository, PressureRepository>();
            services.AddTransient<IDbService, TestDbService>();
            services.AddTransient<ILogger<SensorRepository>, NullLogger<SensorRepository>>();
            services.AddTransient<ILogger<PressureRepository>, NullLogger<PressureRepository>>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            var serviceProvider = services.BuildServiceProvider();
            var sut = CreateSut(serviceProvider);

            // Act
            var actual = sut.CreateRepository(SensorEnum.Pressure)!;
            // Assert
            SensorEnum.Pressure.Repository.IsInstanceOfType(actual).Should().BeTrue();
        }


        [Fact]
        public void CreateRepository_ValidAirTempRepository_ReturnsRepositoryInstance() {
            // Arrange 
            var services = new ServiceCollection();
            services.AddTransient<IAirTempRepository, AirTempRepository>();
            services.AddTransient<IDbService, TestDbService>();
            services.AddTransient<ILogger<SensorRepository>, NullLogger<SensorRepository>>();
            services.AddTransient<ILogger<AirTempRepository>, NullLogger<AirTempRepository>>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            var serviceProvider = services.BuildServiceProvider();
            var sut = CreateSut(serviceProvider);

            // Act
            var actual = sut.CreateRepository(SensorEnum.AirTempHanging)!;
            // Assert
            SensorEnum.AirTempHanging.Repository.IsInstanceOfType(actual).Should().BeTrue();
        }

        [Fact]
        public void CreateRepository_ValidPhRepository_ReturnsRepositoryInstance() {
            // Arrange 
            var services = new ServiceCollection();
            services.AddTransient<IPhRepository, PhRepository>();
            services.AddTransient<IDbService, TestDbService>();
            services.AddTransient<ILogger<SensorRepository>, NullLogger<SensorRepository>>();
            services.AddTransient<ILogger<PhRepository>, NullLogger<PhRepository>>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            var serviceProvider = services.BuildServiceProvider();
            var sut = CreateSut(serviceProvider);

            // Act
            var actual = sut.CreateRepository(SensorEnum.PH)!;
            // Assert
            SensorEnum.PH.Repository.IsInstanceOfType(actual).Should().BeTrue();
        }

        [Fact]
        public void CreateRepository_ValidWaterTempRepository_ReturnsRepositoryInstance() {
            // Arrange 
            var services = new ServiceCollection();
            services.AddTransient<IWaterTemperatureRepository, WaterTemperatureRepository>();
            services.AddTransient<IDbService, TestDbService>();
            services.AddTransient<ILogger<SensorRepository>, NullLogger<SensorRepository>>();
            services.AddTransient<ILogger<WaterTemperatureRepository>, NullLogger<WaterTemperatureRepository>>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            var serviceProvider = services.BuildServiceProvider();
            var sut = CreateSut(serviceProvider);

            // Act
            var actual = sut.CreateRepository(SensorEnum.WaterTemp)!;
            // Assert
            SensorEnum.WaterTemp.Repository.IsInstanceOfType(actual).Should().BeTrue();
        }


        [Fact]
        public void CreateRepository_ValidUvRepository_ReturnsRepositoryInstance() {
            // Arrange 
            var services = new ServiceCollection();
            services.AddTransient<IUvRepository, UvRepository>();
            services.AddTransient<IDbService, TestDbService>();
            services.AddTransient<ILogger<SensorRepository>, NullLogger<SensorRepository>>();
            services.AddTransient<ILogger<UvRepository>, NullLogger<UvRepository>>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            var serviceProvider = services.BuildServiceProvider();
            var sut = CreateSut(serviceProvider);

            // Act
            var actual = sut.CreateRepository(SensorEnum.UV)!;
            // Assert
            SensorEnum.UV.Repository.IsInstanceOfType(actual).Should().BeTrue();
        }

        [Fact]
        public void CreateRepository_ValidTDSRepository_ReturnsRepositoryInstance() {
            // Arrange 
            var services = new ServiceCollection();
            services.AddTransient<ITdsRepository, TdsRepository>();
            services.AddTransient<IDbService, TestDbService>();
            services.AddTransient<ILogger<SensorRepository>, NullLogger<SensorRepository>>();
            services.AddTransient<ILogger<TdsRepository>, NullLogger<TdsRepository>>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            var serviceProvider = services.BuildServiceProvider();
            var sut = CreateSut(serviceProvider);

            // Act
            var actual = sut.CreateRepository(SensorEnum.TDS)!;
            // Assert
            SensorEnum.TDS.Repository.IsInstanceOfType(actual).Should().BeTrue();
        }


        [Fact]
        public void CreateRepository_ValidWaterLevelRepository_ReturnsRepositoryInstance() {
            // Arrange 
            var services = new ServiceCollection();
            services.AddTransient<IWaterLevelRepository, WaterLevelRepository>();
            services.AddTransient<IDbService, TestDbService>();
            services.AddTransient<ILogger<SensorRepository>, NullLogger<SensorRepository>>();
            services.AddTransient<ILogger<WaterLevelRepository>, NullLogger<WaterLevelRepository>>();
            services.AddTransient<ILogger<WaterLevelTypeRepository>, NullLogger<WaterLevelTypeRepository>>();
            services.AddTransient<IWaterLevelTypeRepository, WaterLevelTypeRepository>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            var serviceProvider = services.BuildServiceProvider();
            var sut = CreateSut(serviceProvider);

            // Act
            var actual = sut.CreateRepository(SensorEnum.WaterLevelReservoir)!;
            // Assert
            SensorEnum.WaterLevelReservoir.Repository.IsInstanceOfType(actual).Should().BeTrue();
        }

        [Fact]
        public void CreateRepository_Invalid_ReturnsNull() {
            // Arrange 
            var services = new ServiceCollection();
            services.AddTransient<IGasSensorRepository, GasSensorRepository>();
            services.AddTransient<IDbService, TestDbService>();
            services.AddTransient<ILogger<SensorRepository>, NullLogger<SensorRepository>>();
            services.AddTransient<ILogger<GasSensorRepository>, NullLogger<GasSensorRepository>>();
            var serviceProvider = services.BuildServiceProvider();
            var sut = CreateSut(serviceProvider);

            // Act
            var actual = sut.CreateRepository(SensorEnum.GasSensor)!;
            // Assert
            actual.Should().BeNull();
        }

        private static SensorDataRepositoryFactory CreateSut(IServiceProvider serviceProvider) {
            return new(serviceProvider, NullLogger<SensorDataRepositoryFactory>.Instance);
        }
    }
}