using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Models;
using GardenCore.Models.SensorData;
using GardenDB.Cache.Exceptions;
using GardenDB.Cache.Helper;
using GardenDB.Cache.Services;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Xunit;

namespace GardenDBTest {
    public class CacheServiceTest {
        public static IEnumerable<object[]> Data => new List<object[]> {
            new object[] {new TaskResult()},
            new object[] {new AirQualityModel()},
            new object[] {new PlantHumidityModel()},
            new object[] {new PHModel()},
            new object[] {new TDSModel()},
            new object[] {new TemperatureHumidityModel()},
            new object[] {new TemperatureHumidityCollection()},
            new object[] {new UVLevelModel()},
            new object[] {new WaterLevelModel()},
            new object[] {new WaterTemperatureModel()},
            new object[] {new PressureModel()}
        };


        /// <summary>
        ///     Test register new key/value pair in Cache.
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData("Test")]
        [InlineData(true)]
        [InlineData(1)]
        [InlineData(13094835)]
        [InlineData(int.MaxValue)]
        [InlineData(int.MinValue)]
        [MemberData(nameof(Data))]
        public async Task Register_SaveRandomValueAndTypes_ReturnsTrue(object value) {
            // Arrange 
            var serviceUnderTest = CreateServiceUnderTest();
            // Act
            var save = await serviceUnderTest.Register("TEST", ByteArrayHelper.ObjectToByteArray(value));
            Assert.True(save);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task Register_BoolValues_ReturnsTrue(bool value) {
            // Arrange 
            var serviceUnderTest = CreateServiceUnderTest();
            // Act
            var actual = await serviceUnderTest.Register("TEST", value);
            actual.Should().BeTrue();
        }

        [Theory]
        [InlineData("testTrue", true)]
        [InlineData("testFalse", false)]
        public async Task Read_ReadBooleanValues_ReturnsValue(string key, bool value) {
            // Arrange 
            var serviceUnderTest = CreateServiceUnderTest();
            var saveResult = await serviceUnderTest.Register(key, value);

            // Act
            var actual = await serviceUnderTest.Read(key);

            // Assert
            saveResult.Should().BeTrue();
            actual.Should().Be(value);
        }

        [Fact]
        public async Task Read_KeyDoesNotExist_ThrowsException() {
            // Arrange 
            const string invalidKey = "kd";
            // Act
            Func<Task> act = async () => { await CreateServiceUnderTest().Read(invalidKey); };
            // Assert 
            await act.Should().ThrowAsync<NoCacheEntryFoundException>();
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task ReadObject_ReadRandomObject_ReturnsObjectByteArray(object value) {
            // Arrange 
            var serviceUnderTest = CreateServiceUnderTest();
            var expected = ByteArrayHelper.ObjectToByteArray(value);
            const string key = "key";
            var saveResult = await serviceUnderTest.Register(key, expected);

            // Act
            var actual = await serviceUnderTest.ReadObject(key);

            // Assert
            saveResult.Should().BeTrue();
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task ReadObject_KeyDoesNotExist_ThrowsException() {
            // Arrange 
            const string invalidKey = "kd";
            // Act
            Func<Task> act = async () => { await CreateServiceUnderTest().ReadObject(invalidKey); };
            // Assert 
            await act.Should().ThrowAsync<NoCacheEntryFoundException>();
        }

        [Fact]
        public async Task Remove_KeyDoesNotExist_ReturnsTrue() {
            // Arrange 
            const string invalidKey = "kd";
            // Act
            var actual = await CreateServiceUnderTest().Remove(invalidKey);
            // Assert 
            actual.Should().BeTrue();
        }

        [Fact]
        public async Task Remove_KeyExists_ReturnsTrueKeyNoLongerValid() {
            // Arrange 
            const string key = "kd";
            var sut = CreateServiceUnderTest();
            await sut.Register(key, ByteArrayHelper.ObjectToByteArray("value"));

            // Act
            var actual = await sut.Remove(key);
            Func<Task> act = async () => { await CreateServiceUnderTest().ReadObject(key); };
            // Assert 
            actual.Should().BeTrue();
            await act.Should().ThrowAsync<NoCacheEntryFoundException>();
        }


        /// <summary>
        ///     Create instance of CacheService.
        /// </summary>
        /// <returns></returns>
        private static CacheService CreateServiceUnderTest() {
            var opts = Options.Create(new MemoryDistributedCacheOptions());
            IDistributedCache testCache = new MemoryDistributedCache(opts);
            return new CacheService(testCache);
        }
    }
}