using System;
using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Models;
using GardenDB.Cache.Interfaces;
using GardenDB.Cache.Services;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenDBTest.Cache {
    public class TaskCollectorTest {
        private readonly TaskModel _expectedTaskModel = new() {
            StatusToChangeTo = true,
            TaskFound = true
        };

        [Fact]
        public async Task GetSwitchTask_ValidKey_ReturnsTaskModel() {
            // Arrange 
            var cacheServiceMock = new Mock<ICacheService>();
            cacheServiceMock.Setup(c => c.Read(It.IsAny<string>()))
                .ReturnsAsync(() => true);

            var serviceUnderTest = CreateServiceUnderTest(cacheServiceMock.Object);

            // Act
            var actual = await serviceUnderTest.GetSwitchTask(GardenTaskEnum.None);

            // Assert
            actual.Should().BeEquivalentTo(_expectedTaskModel);
        }


        [Fact]
        public async Task GetSwitchTask_InvalidKey_ReturnsFailedTaskModel() {
            // Arrange 
            var expected = new TaskModel();
            var cacheServiceMock = new Mock<ICacheService>();
            cacheServiceMock.Setup(c => c.Read(It.IsAny<string>()))
                .ThrowsAsync(new Exception());
            var serviceUnderTest = CreateServiceUnderTest(cacheServiceMock.Object);
            // Act
            var actual = await serviceUnderTest.GetSwitchTask(GardenTaskEnum.None);
            // Assert
            actual.Should().BeEquivalentTo(expected);
        }


        [Fact]
        public async Task RemoveSwitchTask_ValidTask_ReturnsTrue() {
            // Arrange
            var cacheServiceMock = new Mock<ICacheService>();
            cacheServiceMock.Setup(c => c.Remove(It.IsAny<string>()))
                .ReturnsAsync(() => true);
            var sut = CreateServiceUnderTest(cacheServiceMock.Object);

            // Act
            var actual = await sut.RemoveSwitchTask(GardenTaskEnum.FanSwitchTask);
            // Assert
            actual.Should().BeTrue();
        }


        [Fact]
        public async Task SetNewState_ValidTask_ReturnsTrue() {
            // Arrange
            var cacheServiceMock = new Mock<ICacheService>();
            cacheServiceMock.Setup(c =>
                    c.Register(It.IsAny<string>(), It.IsAny<bool>()))
                .ReturnsAsync(() => true);
            var sut = CreateServiceUnderTest(cacheServiceMock.Object);

            // Act
            var actual = await sut.SetNewState(GardenTaskEnum.FanSwitchTask, true);

            // Assert
            actual.Should().BeTrue();
        }


        /// <summary>
        ///     Create instance of TaskCollector with a given cacheService.
        /// </summary>
        /// <param name="cacheService"></param>
        /// <returns></returns>
        private static TaskCollector CreateServiceUnderTest(ICacheService cacheService) {
            return new(cacheService, NullLogger<TaskCollector>.Instance);
        }
    }
}