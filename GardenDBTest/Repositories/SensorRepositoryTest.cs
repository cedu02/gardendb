using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenDB.Database.Repositories;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace GardenDBTest.Repositories {
    public class SensorRepositoryTest {
        [Fact]
        public async Task Upsert_ValidData_ReturnsNewId() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            // Act 
            var actual = await sut.Upsert(SensorEnum.GasSensor);
            // Assert 
            actual.Should().Be(1);
        }

        [Fact]
        public async Task Upsert_AllSensors_ReturnsNewIds() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            var sensors = SensorEnum.GetAllSensors();
            // Act 
            List<long> actual = new();
            foreach (var sensor in sensors) actual.Add(await sut.Upsert(sensor));
            // Assert 
            actual.Should().HaveCount(sensors.Count);
            actual.Should().NotContain(0);
        }

        [Fact]
        public async Task GetSensor_ValidSenor_ReturnsSensorInfo() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            // Act
            var expectedId = await sut.Upsert(SensorEnum.GasSensor);
            var actual = await sut.GetSensor(SensorEnum.GasSensor);
            // Assert 
            actual.Id.Should().Be(expectedId);
            actual.Name.Should().Be(SensorEnum.GasSensor.Name);
        }

        [Fact]
        public async Task GetSensor_InvalidSensor_ReturnsEmptySensor() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            // Act
            var actual = await sut.GetSensor(SensorEnum.GasSensor);
            // Assert 
            actual.Id.Should().Be(0);
        }

        private static SensorRepository CreateServiceUnderTest() {
            return new(new TestDbService(), NullLogger<SensorRepository>.Instance);
        }
    }
}