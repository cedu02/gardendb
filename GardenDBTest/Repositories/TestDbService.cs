using System;
using System.Data.Common;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace GardenDBTest.Repositories {
    public class TestDbService : IDbService, IDisposable {
        private readonly DbConnection _connection;
        private readonly DbContextOptions<GardenDbContext> _contextOptions;

        public TestDbService() {
            _contextOptions = new DbContextOptionsBuilder<GardenDbContext>()
                .UseSqlite(CreateInMemoryDatabase())
                .Options;

            _connection = RelationalOptionsExtension.Extract(_contextOptions).Connection;
        }

        public GardenDbContext GetDbContext() {
            var context = new GardenDbContext(_contextOptions);
            CleanCreation(context);
            return context;
        }

        public void Dispose() {
        }

        private static DbConnection CreateInMemoryDatabase() {
            var connection = new SqliteConnection("Filename=:memory:");
            connection.Open();
            return connection;
        }

        private void CleanCreation(GardenDbContext context) {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }
    }
}