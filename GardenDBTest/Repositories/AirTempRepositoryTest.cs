using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenCore.Models.SensorData;
using GardenDB.Database.Repositories;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenDBTest.Repositories {
    public class AirTempRepositoryTest {
        [Fact]
        public async Task SaveNewData_ValidData_ReturnsId() {
            // Arrange 
            var sensorRepoStub = new Mock<ISensorRepository>();
            sensorRepoStub.Setup(s => s.GetSensor(It.IsAny<SensorEnum>()))
                .ReturnsAsync(() => new SensorModel {
                    Id = 1
                });
            var sut = CreateSut(sensorRepoStub.Object);
            // Act 
            var actual = await sut.SaveNewData(SensorEnum.AirTempHanging, new TemperatureHumidityModel());
            // Assert 
            actual.Should().Be(1);
        }


        [Fact]
        public async Task SaveNewData_InvalidDate_ReturnsZero() {
            // Arrange 
            var sensorRepoStub = new Mock<ISensorRepository>();
            sensorRepoStub.Setup(s => s.GetSensor(It.IsAny<SensorEnum>()))
                .ReturnsAsync(() => new SensorModel {
                    Id = 1
                });
            var sut = CreateSut(sensorRepoStub.Object);
            // Act 
            var actual = await sut.SaveNewData(SensorEnum.PH, new UVLevelModel());
            // Assert 
            actual.Should().Be(0);
        }


        private static AirTempRepository CreateSut(ISensorRepository sensorRepository) {
            return new(NullLogger<AirTempRepository>.Instance,
                sensorRepository, new TestDbService());
        }
    }
}