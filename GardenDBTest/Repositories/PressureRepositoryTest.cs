using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenCore.Models.SensorData;
using GardenDB.Database.Repositories;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenDBTest.Repositories {
    public class PressureRepositoryTest {
        [Fact]
        public async Task SaveNewData_ValidData_ReturnsId() {
            // Arrange 
            var sensorRepoStub = new Mock<ISensorRepository>();
            sensorRepoStub.Setup(s => s.GetSensor(It.IsAny<SensorEnum>()))
                .ReturnsAsync(() => new SensorModel {
                    Id = 1
                });
            var sut = CreateSut(sensorRepoStub.Object);
            // Act 
            var actual = await sut.SaveNewData(SensorEnum.Pressure, new PressureModel());
            // Assert 
            actual.Should().Be(1);
        }

        [Fact]
        public async Task SaveNewData_InvalidData_ReturnsId() {
            // Arrange 
            var sensorRepoStub = new Mock<ISensorRepository>();
            sensorRepoStub.Setup(s => s.GetSensor(It.IsAny<SensorEnum>()))
                .ReturnsAsync(() => new SensorModel {
                    Id = 1
                });
            var sut = CreateSut(sensorRepoStub.Object);
            // Act 
            var actual = await sut.SaveNewData(SensorEnum.Pressure, new UVLevelModel());
            // Assert 
            actual.Should().Be(0);
        }

        private static PressureRepository CreateSut(ISensorRepository sensorRepository) {
            return new(NullLogger<PressureRepository>.Instance,
                new TestDbService(),
                sensorRepository);
        }
    }
}