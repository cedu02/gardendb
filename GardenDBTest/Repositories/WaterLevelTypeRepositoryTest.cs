using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenDB.Database.Repositories;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace GardenDBTest.Repositories {
    public class WaterLevelTypeRepositoryTest {
        [Fact]
        public async Task CreateWaterLevelType_ValidType_ReturnsId() {
            // Arrange 
            var sut = CreateSut();
            // Act 
            var actual = await sut.Upsert(WaterLevels.Empty);
            // Assert 
            actual.Should().Be(1);
        }

        [Fact]
        public async Task GetWaterLevelType_ValidType_ReturnsWaterLevelType() {
            // Arrange 
            var sut = CreateSut();
            var id = await sut.Upsert(WaterLevels.Empty);

            // Act 
            var actual = await sut.GetWaterLevelType(WaterLevels.Empty);
            // Assert 
            actual.Id.Should().Be(id);
            actual.Name.Should().Be(WaterLevels.Empty.ToString("F"));
            actual.Number.Should().Be((int) WaterLevels.Empty);
        }

        private static WaterLevelTypeRepository CreateSut() {
            return new(NullLogger<WaterLevelTypeRepository>.Instance,
                new TestDbService());
        }
    }
}