using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenCore.Models.SensorData;
using GardenDB.Database.Repositories;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenDBTest.Repositories {
    public class WaterLevelRepositoryTest {
        [Fact]
        public async Task SaveNewData_ValidData_ReturnsId() {
            // Arrange 
            var sensorRepoStub = new Mock<ISensorRepository>();
            sensorRepoStub.Setup(s => s.GetSensor(It.IsAny<SensorEnum>()))
                .ReturnsAsync(() => new SensorModel {
                    Id = 1
                });
            var waterLevelTypeMock = new Mock<IWaterLevelTypeRepository>();
            waterLevelTypeMock.Setup(w => w.GetWaterLevelType(It.IsAny<WaterLevels>()))
                .ReturnsAsync(() => new WaterLevelTypeModel {
                    Id = 1
                });
            var sut = CreateSut(sensorRepoStub.Object, waterLevelTypeMock.Object);
            // Act 
            var actual = await sut.SaveNewData(SensorEnum.WaterLevelReservoir, new WaterLevelModel() {
                WaterLevel = WaterLevels.Full
            });
            // Assert 
            actual.Should().Be(1);
        }


        [Fact]
        public async Task SaveNewData_InvalidDate_ReturnsZero() {
            // Arrange 
            var sensorRepoStub = new Mock<ISensorRepository>();
            sensorRepoStub.Setup(s => s.GetSensor(It.IsAny<SensorEnum>()))
                .ReturnsAsync(() => new SensorModel {
                    Id = 1
                });
            var waterLevelTypeFake = new Mock<IWaterLevelTypeRepository>();
            var sut = CreateSut(sensorRepoStub.Object, waterLevelTypeFake.Object);
            // Act 
            var actual = await sut.SaveNewData(SensorEnum.WaterLevelReservoir, new UVLevelModel());
            // Assert 
            actual.Should().Be(0);
        }

        private static WaterLevelRepository CreateSut(ISensorRepository sensorRepository,
            IWaterLevelTypeRepository waterLevelTypeRepository) {
            return new(NullLogger<WaterLevelRepository>.Instance,
                sensorRepository, new TestDbService(), waterLevelTypeRepository);
        }
    }
}