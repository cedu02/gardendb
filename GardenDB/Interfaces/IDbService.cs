using GardenDB.Database.Models;

namespace GardenDB.Interfaces {
    public interface IDbService {
        public GardenDbContext GetDbContext();
    }
}