using System;
using System.Linq;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class SensorRepository : ISensorRepository {
        private readonly GardenDbContext _gardenDb;
        private readonly ILogger<SensorRepository> _logger;

        public SensorRepository(IDbService dbService, ILogger<SensorRepository> logger) {
            _logger = logger;
            _gardenDb = dbService.GetDbContext();
        }

        public async Task<int> Upsert(SensorEnum sensorInformation) {
            var sensor = await _gardenDb.Sensor
                .Where(s => s.Name == sensorInformation.Name)
                .SingleOrDefaultAsync() ?? new Sensor();

            sensor.Name = sensorInformation.Name;
            sensor.Active = true;
            sensor.TaskNumber = sensorInformation.DataTask.ActionCode;

            try {
                _gardenDb.Sensor.Update(sensor);
                await _gardenDb.SaveChangesAsync();
                _logger.LogDebug("Updated or changed sensor {schedule}", sensor);
                return sensor.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to create or update sensor");
                return 0;
            }
        }

        public async Task<SensorModel> GetSensor(SensorEnum sensor) {
            var resultSensor = await _gardenDb.Sensor
                .Where(s => s.Name == sensor.Name)
                .Select(s => new SensorModel {
                    Id = s.Id,
                    Name = s.Name,
                    Active = s.Active,
                    SensorTypeId = s.SensorTypeId,
                    TaskNumber = s.TaskNumber
                })
                .SingleOrDefaultAsync() ?? new SensorModel();

            _logger.LogDebug("Found sensor {sensor}", resultSensor);

            return resultSensor;
        }
    }
}