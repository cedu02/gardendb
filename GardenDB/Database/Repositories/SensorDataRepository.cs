using GardenCore.Enums;

namespace GardenDB.Database.Repositories {
    public static class SensorDataRepository {
        public static bool CheckTypeAndNull(SensorEnum sensor, object? data) {
            if (data == null) return false;

            if (data.GetType() != sensor.DataTask.DataType) return false;

            return true;
        }
    }
}