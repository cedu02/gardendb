using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class WaterTemperatureRepository : IWaterTemperatureRepository, ISensorDataRepository {
        private readonly IDbService _dbService;

        private readonly ILogger<WaterTemperatureRepository> _logger;
        private readonly ISensorRepository _sensorRepository;

        public WaterTemperatureRepository(ILogger<WaterTemperatureRepository> logger,
            ISensorRepository sensorRepository,
            IDbService dbService) {
            _dbService = dbService;
            _sensorRepository = sensorRepository;
            _logger = logger;
        }

        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            if (!SensorDataRepository.CheckTypeAndNull(sensor, data)) {
                _logger.LogError("Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data.GetType());
                return 0;
            }

            WaterTemperatureModel waterTempData;
            try {
                waterTempData = (WaterTemperatureModel) data;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Invalid data provided for {class} {data}",
                    nameof(WaterTemperatureRepository), data);
                return 0;
            }

            var sensorInfo = await _sensorRepository.GetSensor(sensor);

            var item = new WaterTemperature {
                SensorId = sensorInfo.Id,
                Timestamp = waterTempData.TimeStamp,
                Kelvin = waterTempData.Temp
            };

            try {
                var db = _dbService.GetDbContext();
                await db.WaterTemperature.AddAsync(item);
                await db.SaveChangesAsync();
                _logger.LogDebug("Saved new air quality data {id}", item.Id);
                return item.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to save new  water temp data {data}",
                    waterTempData);
                return 0;
            }
        }
    }
}