using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class PhRepository : ISensorDataRepository, IPhRepository {
        private readonly IDbService _dbService;

        private readonly ILogger<PhRepository> _logger;
        private readonly ISensorRepository _sensorRepository;

        public PhRepository(ILogger<PhRepository> logger,
            ISensorRepository sensorRepository,
            IDbService dbService) {
            _logger = logger;
            _dbService = dbService;
            _sensorRepository = sensorRepository;
        }

        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            if (!SensorDataRepository.CheckTypeAndNull(sensor, data)) {
                _logger.LogError("Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data.GetType());
                return 0;
            }

            PHModel phData;
            try {
                phData = (PHModel) data;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data);
                return 0;
            }

            var sensorInfo = await _sensorRepository.GetSensor(sensor);

            var item = new PH {
                SensorId = sensorInfo.Id,
                Timestamp = phData.TimeStamp,
                PHLevel = phData.PH
            };

            try {
                var db = _dbService.GetDbContext();
                await db.PH.AddAsync(item);
                await db.SaveChangesAsync();
                _logger.LogDebug("Saved new ph data {id}", item.Id);
                return item.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to save new ph data {data}", phData);
                return 0;
            }
        }
    }
}