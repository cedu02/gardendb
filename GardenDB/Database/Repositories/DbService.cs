using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace GardenDB.Database.Repositories {
    public class DbService : IDbService {
        private const string DB_NAME = "garden.db";

        private readonly IServiceScopeFactory _scopeFactory;

        public DbService(IServiceScopeFactory scopeFactory) {
            _scopeFactory = scopeFactory;
        }

        /**
         * Get dbcontext connection to db.
         */
        public GardenDbContext GetDbContext() {
            return _scopeFactory.CreateScope().ServiceProvider.GetService<GardenDbContext>();
        }
    }
}