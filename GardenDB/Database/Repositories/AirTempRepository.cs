using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class AirTempRepository : IAirTempRepository {
        private readonly IDbService _dbService;

        private readonly ILogger<AirTempRepository> _logger;
        private readonly ISensorRepository _sensorRepository;

        public AirTempRepository(ILogger<AirTempRepository> logger,
            ISensorRepository sensorRepository,
            IDbService dbService) {
            _logger = logger;
            _dbService = dbService;
            _sensorRepository = sensorRepository;
        }

        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            if (!SensorDataRepository.CheckTypeAndNull(sensor, data)) {
                _logger.LogError("Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data.GetType());
                return 0;
            }

            TemperatureHumidityModel tempData;
            try {
                tempData = (TemperatureHumidityModel) data;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data);
                return 0;
            }

            var sensorInfo = await _sensorRepository.GetSensor(sensor);

            var item = new AirTemperature {
                SensorId = sensorInfo.Id,
                Timestamp = tempData.TimeStamp,
                Kelvin = tempData.Temp,
                Humidity = tempData.Humidity
            };

            try {
                var db = _dbService.GetDbContext();
                await db.AirTemperature.AddAsync(item);
                await db.SaveChangesAsync();
                _logger.LogDebug("Saved new air temp data {id}", item.Id);
                return item.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to save new air temp data {data}", tempData);
                return 0;
            }
        }
    }
}