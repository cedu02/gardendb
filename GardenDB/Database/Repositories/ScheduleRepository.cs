using System;
using System.Linq;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class ScheduleRepository : IScheduleRepository {
        private readonly GardenDbContext _gardenDb;
        private readonly ILogger<ScheduleRepository> _logger;

        private readonly IScheduleTypeRepository _scheduleTypeRepository;

        public ScheduleRepository(IDbService dbService,
            ILogger<ScheduleRepository> logger,
            IScheduleTypeRepository scheduleTypeRepository) {
            _gardenDb = dbService.GetDbContext();
            _logger = logger;
            _scheduleTypeRepository = scheduleTypeRepository;
        }

        public async Task<ScheduleModel> GetSchedule(ScheduleTypeEnum scheduleType) {
            _logger.LogDebug("Get active Schedule for ActionTask: {task}", scheduleType);

            var sqlQuery = from schedule in _gardenDb.Schedule
                join sType in _gardenDb.ScheduleType
                    on schedule.ScheduleTypeId equals sType.Code
                where schedule.ActiveFrom <= DateTime.Now &&
                      schedule.ActiveTo >= DateTime.Now &&
                      sType.Code == scheduleType.ScheduleTypeCode
                select new ScheduleModel {
                    Id = schedule.Id,
                    Interval = schedule.Interval,
                    Duration = schedule.Duration,
                    ActiveFrom = schedule.ActiveFrom,
                    ActiveTo = schedule.ActiveTo,
                    ScheduleType = scheduleType
                };

            try {
                var result = await sqlQuery.SingleOrDefaultAsync() ?? new ScheduleModel();
                _logger.LogDebug("Active schedule for ActionTask: {actionTask} -> {schedule}",
                    scheduleType,
                    result);
                return result;
            }
            catch (Exception ex) {
                _logger.LogError("Failed to select schedule", ex);
                return new ScheduleModel();
            }
        }


        /**
         * Add or Update Schedule from
         */
        public async Task<TaskResult> UpsertSchedule(ScheduleModel schedule) {
            if (schedule.Id != 0) return await UpdateSchedule(schedule);

            return await SaveNewSchedule(schedule);
        }

        /// <summary>
        ///     Update schedule.
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        private async Task<TaskResult> UpdateSchedule(ScheduleModel schedule) {
            _logger.LogDebug("Update existing schedule. {param}", schedule);

            var scheduleItem = await _gardenDb.Schedule
                .Where(s => s.Id == schedule.Id)
                .SingleOrDefaultAsync();

            scheduleItem.Interval = schedule.Interval;
            scheduleItem.ActiveFrom = schedule.ActiveFrom;
            scheduleItem.ActiveTo = schedule.ActiveTo;
            _gardenDb.Schedule.Update(scheduleItem);

            try {
                _logger.LogDebug("Try to update schedule", scheduleItem);
                await _gardenDb.SaveChangesAsync();
                _logger.LogDebug("Update schedule successful", scheduleItem);
                return new TaskResult {
                    Success = true,
                    Message = "Saved schedule."
                };
            }
            catch {
                _logger.LogError("Failed to update schedule.", scheduleItem);
                return new TaskResult {
                    Success = false,
                    Message = "Failed to create schedule."
                };
            }
        }

        /// <summary>
        ///     Save new schedule.
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        private async Task<TaskResult> SaveNewSchedule(ScheduleModel schedule) {
            _logger.LogDebug("Create new schedule. {param}", schedule);

            _logger.LogDebug("Select ScheduleType for schedule", schedule.ScheduleType);
            var scheduleType =
                await _scheduleTypeRepository.GetScheduleTypeByCode(schedule.ScheduleType.ScheduleTypeCode);

            if (scheduleType.ScheduleType == ScheduleTypeEnum.InvalidSchedule) {
                _logger.LogError("ScheduleType not found", schedule.ScheduleType);
                return new TaskResult {
                    Success = false,
                    Message = "ScheduleType not found"
                };
            }

            _logger.LogDebug("Schedule type found:", scheduleType);

            var newSchedule = new Schedule {
                ActiveFrom = schedule.ActiveFrom,
                ActiveTo = schedule.ActiveTo,
                ScheduleTypeId = scheduleType.Id,
                Interval = schedule.Interval
            };

            await _gardenDb.Schedule.AddAsync(newSchedule);
            try {
                _logger.LogDebug("Save new schedule to database");
                await _gardenDb.SaveChangesAsync();
                _logger.LogDebug("New schedules created successful");
                return new TaskResult {
                    Success = true,
                    Message = "Saved schedule."
                };
            }
            catch {
                _logger.LogError("Failed to save new schedule.");
                return new TaskResult {
                    Success = false,
                    Message = "Failed to create schedule."
                };
            }
        }
    }
}