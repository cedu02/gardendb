using System;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class SensorDataRepositoryFactory : ISensorDataRepositoryFactory {
        private readonly ILogger<SensorDataRepositoryFactory> _logger;

        private readonly IServiceProvider _provider;

        public SensorDataRepositoryFactory(IServiceProvider provider,
            ILogger<SensorDataRepositoryFactory> logger) {
            _provider = provider;
            _logger = logger;
        }

        public ISensorDataRepository? CreateRepository(SensorEnum sensor) {
            try {
                var newInstance = (ISensorDataRepository) _provider.GetRequiredService(sensor.Repository);
                return newInstance;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to create repository for sensor {sensor}", sensor);
                return null;
            }
        }
    }
}