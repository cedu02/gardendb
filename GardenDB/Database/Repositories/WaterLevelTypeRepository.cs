using System;
using System.Linq;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class WaterLevelTypeRepository : IWaterLevelTypeRepository {
        private readonly IDbService _dbService;

        private readonly ILogger<WaterLevelTypeRepository> _logger;

        public WaterLevelTypeRepository(ILogger<WaterLevelTypeRepository> logger, IDbService dbService) {
            _logger = logger;
            _dbService = dbService;
        }

        public async Task<int> Upsert(WaterLevels waterLevel) {
            var context = _dbService.GetDbContext();
            var waterLevelType = await context.WaterLevelType
                .Where(s => s.Number == (int) waterLevel)
                .SingleOrDefaultAsync() ?? new WaterLevelType();

            waterLevelType.Number = (int) waterLevel;
            waterLevelType.Name = waterLevel.ToString("F");

            try {
                context.WaterLevelType.Update(waterLevelType);
                await context.SaveChangesAsync();
                return waterLevelType.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to upsert water level type {type}", waterLevel);
                return 0;
            }
        }

        public Task<WaterLevelTypeModel> GetWaterLevelType(WaterLevels waterLevel) {
            var context = _dbService.GetDbContext();
            var result = context.WaterLevelType
                .Where(w => w.Number == (int) waterLevel)
                .Select(w => new WaterLevelTypeModel {
                    Id = w.Id,
                    Name = w.Name,
                    Number = w.Number
                })
                .SingleOrDefaultAsync();
            return result;
        }
    }
}