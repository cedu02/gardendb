using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class PressureRepository : ISensorDataRepository, IPressureRepository {
        private readonly IDbService _dbService;

        private readonly ILogger<PressureRepository> _logger;
        private readonly ISensorRepository _sensorRepository;

        public PressureRepository(ILogger<PressureRepository> logger,
            IDbService dbService,
            ISensorRepository sensorRepository) {
            _logger = logger;
            _dbService = dbService;
            _sensorRepository = sensorRepository;
        }

        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            if (!SensorDataRepository.CheckTypeAndNull(sensor, data)) {
                _logger.LogError("Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data.GetType());
                return 0;
            }

            PressureModel pressureData;
            try {
                pressureData = (PressureModel) data;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data);
                return 0;
            }

            var sensorInfo = await _sensorRepository.GetSensor(sensor);

            var item = new Pressure {
                SensorId = sensorInfo.Id,
                Timestamp = pressureData.TimeStamp,
                Pascal = pressureData.Pressure,
                Temp = pressureData.Temp,
                Humidity = pressureData.Humidity,
                Gas = pressureData.Gas,
                ApproxAltitude = pressureData.ApproxAltitude
            };

            try {
                var db = _dbService.GetDbContext();
                await db.Pressure.AddAsync(item);
                await db.SaveChangesAsync();
                _logger.LogDebug("Saved new pressure data {id}", item);
                return item.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to save new  pressure data {data}", pressureData);
                return 0;
            }
        }
    }
}