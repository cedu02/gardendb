using System;
using System.Linq;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class GardenTaskRepository : IGardenTaskRepository {
        private readonly GardenDbContext _gardenDb;

        private readonly IGardenTaskTypeRepository _gardenTaskTypeRepository;
        private readonly ILogger<GardenTaskRepository> _logger;

        public GardenTaskRepository(IDbService dbService,
            ILogger<GardenTaskRepository> logger,
            IGardenTaskTypeRepository gardenTaskTypeRepository) {
            _gardenDb = dbService.GetDbContext();
            _logger = logger;
            _gardenTaskTypeRepository = gardenTaskTypeRepository;
        }

        public async Task<GardenTaskModel> GetLatestActionForType(ScheduleTypeEnum scheduleType) {
            _logger.LogInformation("Get latest action for ActionTask: {actionTypeCode}", scheduleType);

            var sqlQuery = from action in _gardenDb.GardenTask
                join actionType in _gardenDb.GardenTaskType
                    on action.GardenTaskTypeId equals actionType.Id
                where actionType.Code == scheduleType.StopAction.ActionCode ||
                      actionType.Code == scheduleType.StopAction.ActionCode
                orderby action.Timestamp
                select new GardenTaskModel {
                    Timestamp = action.Timestamp,
                    ArduinoTask = ArduinoTaskEnum.GetActionTypeByCode(actionType.Code),
                    ScheduleId = action.ScheduleId
                };

            try {
                var result = await sqlQuery.SingleOrDefaultAsync() ?? new GardenTaskModel();
                _logger.LogInformation("Selected action: {Action}", result);
                return result;
            }
            catch (Exception ex) {
                _logger.LogError("Failed to select latest action for action type", ex);
                return new GardenTaskModel();
            }
        }

        /// <summary>
        ///     Select latest executed action for a specific schedule.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<GardenTaskModel> GetLatestActionForSchedule(ScheduleModel param) {
            _logger.LogInformation("Get latest action for Schedule: {schedule}", param);

            var sqlQuery = from action in _gardenDb.GardenTask
                join schedule in _gardenDb.Schedule
                    on action.ScheduleId equals schedule.Id
                where schedule.Id == param.Id
                orderby action.Timestamp
                select new GardenTaskModel {
                    Timestamp = action.Timestamp,
                    ScheduleId = schedule.Id,
                    ArduinoTask = ArduinoTaskEnum.GetActionTypeByCode(action.GardenTaskType.Code)
                };

            try {
                var result = await sqlQuery.SingleOrDefaultAsync() ?? new GardenTaskModel();
                _logger.LogInformation("Selected action: {Action}", result);
                return result;
            }
            catch (Exception ex) {
                _logger.LogError("Failed to select latest action for schedule", ex);
                return new GardenTaskModel();
            }
        }

        public async Task<TaskResult> SaveAction(GardenTaskModel newGardenTask) {
            _logger.LogInformation("Get actionTypeId of action: {action}", newGardenTask);

            var actionTypeId = await _gardenTaskTypeRepository
                .GetActionTypeIdByActionCode(newGardenTask.ArduinoTask.ActionCode);

            _logger.LogInformation("Selected actionTypeId: {id}", actionTypeId);

            var action = new GardenTask {
                Timestamp = DateTime.Now,
                GardenTaskTypeId = actionTypeId,
                ScheduleId = newGardenTask.ScheduleId
            };

            _logger.LogInformation("Save new Action: {action}", action);
            try {
                await _gardenDb.AddAsync(action);
                await _gardenDb.SaveChangesAsync();
                _logger.LogInformation("Successfully saved new Action: {action}", action);
                return new TaskResult {
                    Success = true,
                    Message = "Saved new action."
                };
            }
            catch (Exception ex) {
                _logger.LogError("Save new Action failed", ex);
                return new TaskResult {
                    Success = false,
                    Message = ex.Message
                };
            }
        }
    }
}