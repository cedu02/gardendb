using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class GasSensorRepository : ISensorDataRepository, IGasSensorRepository {
        private readonly IDbService _dbService;

        private readonly ILogger<GasSensorRepository> _logger;
        private readonly ISensorRepository _sensorRepository;

        public GasSensorRepository(ILogger<GasSensorRepository> logger,
            IDbService dbService,
            ISensorRepository sensorRepository) {
            _logger = logger;
            _dbService = dbService;
            _sensorRepository = sensorRepository;
        }

        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            if (!SensorDataRepository.CheckTypeAndNull(sensor, data)) {
                _logger.LogError("Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data.GetType());
                return 0;
            }

            AirQualityModel gasData;
            try {
                gasData = (AirQualityModel) data;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data);
                return 0;
            }

            var sensorInfo = await _sensorRepository.GetSensor(sensor);

            var item = new AirQuality {
                SensorId = sensorInfo.Id,
                Timestamp = gasData.TimeStamp,
                CO2 = gasData.Co2,
                TVOC = gasData.TVOC
            };

            try {
                var db = _dbService.GetDbContext();
                await db.AirQuality.AddAsync(item);
                await db.SaveChangesAsync();
                _logger.LogDebug("Saved new air quality data {id}", item.Id);
                return item.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to save new  air quality data {data}", gasData);
                return 0;
            }
        }
    }
}