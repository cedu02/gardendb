using System;
using System.Linq;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class SensorTypeRepository : ISensorTypeRepository {
        private readonly GardenDbContext _gardenDb;
        private readonly ILogger<SensorTypeRepository> _logger;

        public SensorTypeRepository(IDbService dbService, ILogger<SensorTypeRepository> logger) {
            _logger = logger;
            _gardenDb = dbService.GetDbContext();
        }

        public async Task<long> Upsert(SensorTypeEnum type) {
            var sensorType = await _gardenDb.SensorType
                .Where(s => s.Type == type.Type)
                .SingleOrDefaultAsync() ?? new SensorType();

            sensorType.Type = type.Type;
            sensorType.Name = type.Name;

            try {
                _gardenDb.SensorType.Update(sensorType);
                await _gardenDb.SaveChangesAsync();
                _logger.LogDebug("Updated or changed sensor type {schedule}", sensorType);
                return sensorType.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to create or update sensor type");
                return 0;
            }
        }
    }
}