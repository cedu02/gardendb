using System;
using System.Linq;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class ScheduleTypeRepository : IScheduleTypeRepository {
        private readonly GardenDbContext _gardenDb;
        private readonly ILogger<ScheduleTypeRepository> _logger;

        public ScheduleTypeRepository(IDbService dbService, ILogger<ScheduleTypeRepository> logger) {
            _logger = logger;
            _gardenDb = dbService.GetDbContext();
        }

        public async Task<ScheduleTypeModel> GetScheduleTypeByCode(int code) {
            _logger.LogDebug("Get ScheduleType by code {code}", code);

            var sqlQuery = from scheduleType in _gardenDb.ScheduleType
                where scheduleType.Code == code
                select new ScheduleTypeModel {
                    Id = scheduleType.Id,
                    ScheduleType = ScheduleTypeEnum.GetScheduleTypeByCode(code)
                };

            try {
                var result = await sqlQuery.SingleOrDefaultAsync() ?? new ScheduleTypeModel {
                    Id = 0,
                    ScheduleType = ScheduleTypeEnum.InvalidSchedule
                };

                if (result.ScheduleType == ScheduleTypeEnum.InvalidSchedule)
                    _logger.LogWarning("Schedule type not found.");

                _logger.LogDebug("ScheduleType found {scheduleType}", result);
                return result;
            }
            catch (Exception ex) {
                _logger.LogError("Schedule type not found.", ex);
                return new ScheduleTypeModel {
                    Id = 0,
                    ScheduleType = ScheduleTypeEnum.InvalidSchedule
                };
            }
        }

        public async Task<long> Upsert(ScheduleTypeEnum schedule) {
            var scheduleType = await _gardenDb.ScheduleType
                .Where(s => s.Code == schedule.ScheduleTypeCode)
                .SingleOrDefaultAsync() ?? new ScheduleType();

            scheduleType.Code = schedule.ScheduleTypeCode;
            scheduleType.Name = schedule.Name;

            try {
                _gardenDb.ScheduleType.Update(scheduleType);
                await _gardenDb.SaveChangesAsync();
                _logger.LogDebug("Updated or changed schedule type {schedule}", scheduleType);
                return scheduleType.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to create or update schedule type");
                return 0;
            }
        }
    }
}