using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class UvRepository : IUvRepository {
        private readonly IDbService _dbService;
        private readonly ILogger<UvRepository> _logger;
        private readonly ISensorRepository _sensorRepository;

        public UvRepository(ILogger<UvRepository> logger,
            ISensorRepository sensorRepository,
            IDbService dbService) {
            _logger = logger;
            _dbService = dbService;
            _sensorRepository = sensorRepository;
        }

        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            if (!SensorDataRepository.CheckTypeAndNull(sensor, data)) {
                _logger.LogError("Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data.GetType());
                return 0;
            }

            UVLevelModel uvLevel;
            try {
                uvLevel = (UVLevelModel) data;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data);
                return 0;
            }

            var sensorInfo = await _sensorRepository.GetSensor(sensor);

            var item = new UVLight {
                SensorId = sensorInfo.Id,
                Timestamp = uvLevel.TimeStamp,
                UVLightLevel = uvLevel.UVLevel
            };

            try {
                var db = _dbService.GetDbContext();
                await db.UVLight.AddAsync(item);
                await db.SaveChangesAsync();
                _logger.LogDebug("Saved new uv light data {id}", item.Id);
                return item.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to save new uv light data {data}", uvLevel);
                return 0;
            }
        }
    }
}