using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class TdsRepository : ISensorDataRepository, ITdsRepository {
        private readonly IDbService _dbService;


        private readonly ILogger<TdsRepository> _logger;
        private readonly ISensorRepository _sensorRepository;

        public TdsRepository(ILogger<TdsRepository> logger,
            ISensorRepository sensorRepository,
            IDbService dbService) {
            _logger = logger;
            _dbService = dbService;
            _sensorRepository = sensorRepository;
        }

        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            if (!SensorDataRepository.CheckTypeAndNull(sensor, data)) {
                _logger.LogError("Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data.GetType());
                return 0;
            }

            TDSModel tdsData;
            try {
                tdsData = (TDSModel) data;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data);
                return 0;
            }

            var sensorInfo = await _sensorRepository.GetSensor(sensor);

            var item = new TDS {
                SensorId = sensorInfo.Id,
                Timestamp = tdsData.TimeStamp,
                TdsMeasurements = tdsData.TDS,
                EC = tdsData.EC
            };

            try {
                var db = _dbService.GetDbContext();
                await db.TDS.AddAsync(item);
                await db.SaveChangesAsync();
                _logger.LogDebug("Saved new tds data {id}", item.Id);
                return item.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to save new tds data {data}", tdsData);
                return 0;
            }
        }
    }
}