using System;
using System.Linq;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class GardenTaskTypeRepository : IGardenTaskTypeRepository {
        private readonly GardenDbContext _gardenDb;
        private readonly ILogger<GardenTaskTypeRepository> _logger;

        public GardenTaskTypeRepository(IDbService dbService, ILogger<GardenTaskTypeRepository> logger) {
            _gardenDb = dbService.GetDbContext();
            _logger = logger;
        }

        public async Task<int> GetActionTypeIdByActionCode(long actionCode) {
            _logger.LogDebug("Get actionTypeId from action code: {actionCode}", actionCode);

            try {
                var actionTypeId = await _gardenDb.GardenTaskType
                    .Where(a => a.Code == actionCode)
                    .Select(a => a.Id)
                    .SingleOrDefaultAsync();
                _logger.LogDebug("ActionTypeId selected: {actionTypeId}", actionTypeId);
                return actionTypeId;
            }
            catch (Exception ex) {
                _logger.LogError("Failed to select actionTypeId", ex);
                return 0;
            }
        }

        public async Task<int> Upsert(ArduinoTaskEnum actionType) {
            var newActionType = await _gardenDb.GardenTaskType
                .Where(a => a.Code == actionType.ActionCode)
                .SingleOrDefaultAsync() ?? new GardenTaskType();

            newActionType.Code = actionType.ActionCode;
            newActionType.Name = actionType.Value;

            try {
                _gardenDb.GardenTaskType.Update(newActionType);
                await _gardenDb.SaveChangesAsync();
                _logger.LogDebug("Updated or changed action type {newActionType}", newActionType);
                return newActionType.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to create or update action type");
                return 0;
            }
        }
    }
}