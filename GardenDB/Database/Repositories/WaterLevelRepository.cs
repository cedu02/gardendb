using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class WaterLevelRepository : IWaterLevelRepository {
        private readonly IDbService _dbService;

        private readonly ILogger<WaterLevelRepository> _logger;
        private readonly ISensorRepository _sensorRepository;
        private readonly IWaterLevelTypeRepository _waterLevelTypeRepository;

        public WaterLevelRepository(ILogger<WaterLevelRepository> logger,
            ISensorRepository sensorRepository,
            IDbService dbService, IWaterLevelTypeRepository waterLevelTypeRepository) {
            _logger = logger;
            _dbService = dbService;
            _sensorRepository = sensorRepository;
            _waterLevelTypeRepository = waterLevelTypeRepository;
        }

        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            if (!SensorDataRepository.CheckTypeAndNull(sensor, data)) {
                _logger.LogError("Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data.GetType());
                return 0;
            }

            WaterLevelModel waterLevelData;
            try {
                waterLevelData = (WaterLevelModel) data;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data);
                return 0;
            }

            if (waterLevelData.WaterLevel == WaterLevels.None) {
                return 0; 
            }

            var waterLevelType = await _waterLevelTypeRepository.GetWaterLevelType(waterLevelData.WaterLevel);
            var sensorInfo = await _sensorRepository.GetSensor(sensor);

            var item = new WaterLevel {
                SensorId = sensorInfo.Id,
                Timestamp = waterLevelData.TimeStamp,
                WaterLevelTypeId = waterLevelType.Id
            };

            try {
                var db = _dbService.GetDbContext();
                await db.WaterLevel.AddAsync(item);
                await db.SaveChangesAsync();
                _logger.LogDebug("Saved new water level data {id}", item.Id);
                return item.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to save new water level data {data}", waterLevelData);
                return 0;
            }
        }
    }
}