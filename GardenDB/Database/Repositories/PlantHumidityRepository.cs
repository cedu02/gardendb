using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models.SensorData;
using GardenDB.Database.Models;
using GardenDB.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Database.Repositories {
    public class PlantHumidityRepository : IPlantHumidityRepository {
        private readonly IDbService _dbService;

        private readonly ILogger<PlantHumidityRepository> _logger;
        private readonly ISensorRepository _sensorRepository;

        public PlantHumidityRepository(ILogger<PlantHumidityRepository> logger,
            ISensorRepository sensorRepository,
            IDbService dbService) {
            _logger = logger;
            _dbService = dbService;
            _sensorRepository = sensorRepository;
        }

        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            if (data.GetType() != sensor.DataTask.DataType) {
                _logger.LogError("Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data.GetType());
                return 0;
            }

            PlantHumidityModel humidityData;
            try {
                humidityData = (PlantHumidityModel) data;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Invalid data provided for {class} {data}",
                    nameof(GasSensorRepository), data);
                return 0;
            }

            var sensorInfo = await _sensorRepository.GetSensor(sensor);

            float humidity;

            if (sensor == SensorEnum.PlantHumidity1)
                humidity = humidityData.Sensor0;
            else if (sensor == SensorEnum.PlantHumidity2)
                humidity = humidityData.Sensor1;
            else if (sensor == SensorEnum.PlantHumidity3)
                humidity = humidityData.Sensor2;
            else
                humidity = humidityData.Sensor3;

            var item = new GroundHumidity {
                SensorId = sensorInfo.Id,
                Timestamp = humidityData.TimeStamp,
                Humidity = humidity
            };

            try {
                var db = _dbService.GetDbContext();
                await db.GroundHumidity.AddAsync(item);
                await db.SaveChangesAsync();
                _logger.LogDebug("Saved new ph data {id}", item.Id);
                return item.Id;
            }
            catch (Exception ex) {
                _logger.LogError(ex, "Failed to save new ph data {data}", humidityData);
                return 0;
            }
        }
    }
}