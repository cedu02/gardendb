using System;

namespace GardenDB.Database.Models {
    public class PH {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public float PHLevel { get; set; }
        public int SensorId { get; set; }
        public Sensor Sensor { get; set; } = new();
    }
}