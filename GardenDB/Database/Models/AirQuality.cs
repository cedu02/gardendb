using System;

namespace GardenDB.Database.Models {
    public class AirQuality {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public int IAQ { get; set; }
        public long CO2 { get; set; }
        public long TVOC { get; set; }
        public int SensorId { get; set; }
        public Sensor Sensor { get; set; } = new();
    }
}