using System;

namespace GardenDB.Database.Models {
    public class Action {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public int ActionTypeId { get; set; }
        public int ScheduleId { get; set; }
        public ActionType ActionType { get; set; } = new();
        public Schedule Schedule { get; set; } = new();
    }
}