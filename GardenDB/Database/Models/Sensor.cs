using System.Collections.Generic;

namespace GardenDB.Database.Models {
    public class Sensor {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;

        public bool Active { get; set; }

        public int TaskNumber { get; set; }

        public int SensorTypeId { get; set; }
        public SensorType SensorType { get; set; } = new();
        public List<AirTemperature> AirTemperatures { get; set; } = new();
        public List<AirQuality> AirQualities { get; set; } = new();
        public List<WaterLevel> WaterLevels { get; set; } = new();
        public List<GroundHumidity> GroundHumidities { get; set; } = new();
        public List<LUX> LuxMeasurements { get; set; } = new();
        public List<PH> PHLevels { get; set; } = new();
        public List<Pressure> Pressures { get; set; } = new();
        public List<TDS> TdsMeasurements { get; set; } = new();
        public List<UVLight> UVLightLevels { get; set; } = new();
        public List<WaterTemperature> WaterTemperatures { get; set; } = new();
    }
}