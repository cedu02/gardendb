using System.Collections.Generic;

namespace GardenDB.Database.Models {
    public class WaterLevelType {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public int Number { get; set; }
        public List<WaterLevel> WaterLevels { get; set; } = new();
    }
}