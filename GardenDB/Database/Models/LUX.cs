using System;

namespace GardenDB.Database.Models {
    public class LUX {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public long LUXMesurment { get; set; }
        public int SensorId { get; set; }
        public Sensor Sensor { get; set; } = new();
    }
}