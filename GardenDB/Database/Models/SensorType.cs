using System.Collections.Generic;

namespace GardenDB.Database.Models {
    public class SensorType {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; } = string.Empty;
        public List<Sensor> Sensors { get; set; } = new();
    }
}