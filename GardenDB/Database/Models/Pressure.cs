using System;

namespace GardenDB.Database.Models {
    public class Pressure {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public decimal Pascal { get; set; }

        public float Temp { get; set; }
        public float Humidity { get; set; }
        public int Gas { get; set; }
        public float ApproxAltitude { get; set; }
        public int SensorId { get; set; }
        public Sensor Sensor { get; set; } = new();
    }
}