using System;

namespace GardenDB.Database.Models {
    public class WaterLevel {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }


        public int WaterLevelTypeId { get; set; }
        public WaterLevelType WaterLevelType { get; set; } = new();

        public int SensorId { get; set; }
        public Sensor Sensor { get; set; } = new();
    }
}