using Microsoft.EntityFrameworkCore;

namespace GardenDB.Database.Models {
    public class GardenDbContext : DbContext {
        public GardenDbContext(DbContextOptions<GardenDbContext> options)
            : base(options) {
        }

        public GardenDbContext() {
        }

        public DbSet<Sensor> Sensor { get; set; } = null!;
        public DbSet<SensorType> SensorType { get; set; } = null!;
        public DbSet<AirTemperature> Temperature { get; set; } = null!;
        public DbSet<User> User { get; set; } = null!;
        public DbSet<WaterLevel> WaterLevel { get; set; } = null!;
        public DbSet<WaterLevelType> WaterLevelType { get; set; } = null!;
        public DbSet<AirQuality> AirQuality { get; set; } = null!;
        public DbSet<AirTemperature> AirTemperature { get; set; } = null!;

        public DbSet<LUX> LUX { get; set; } = null!;
        public DbSet<PH> PH { get; set; } = null!;
        public DbSet<GroundHumidity> GroundHumidity { get; set; } = null!;
        public DbSet<Plant> Plant { get; set; } = null!;
        public DbSet<Pressure> Pressure { get; set; } = null!;
        public DbSet<TDS> TDS { get; set; } = null!;
        public DbSet<UVLight> UVLight { get; set; } = null!;
        public DbSet<WaterTemperature> WaterTemperature { get; set; } = null!;
        public DbSet<GardenTask> GardenTask { get; set; } = null!;
        public DbSet<GardenTaskType> GardenTaskType { get; set; } = null!;
        public DbSet<Schedule> Schedule { get; set; } = null!;
        public DbSet<ScheduleType> ScheduleType { get; set; } = null!;


        protected override void OnConfiguring(DbContextOptionsBuilder options) {
            if (!options.IsConfigured) options.UseSqlite("Data Source=garden.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Sensor>(sn => {
                sn.Property(b => b.Name).HasColumnType("varchar(200)");
                sn.HasOne(b => b.SensorType)
                    .WithMany(t => t.Sensors)
                    .HasForeignKey(b => b.SensorTypeId)
                    .IsRequired();
            });

            modelBuilder.Entity<SensorType>(sn => { sn.Property(b => b.Type).HasColumnType("varchar(200)"); });

            modelBuilder.Entity<Plant>(sn => {
                sn.Property(b => b.Name).HasColumnType("varchar(200)");
                sn.Property(b => b.Species).HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<AirTemperature>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.AirTemperatures)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });

            modelBuilder.Entity<Pressure>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.Pressures)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });

            modelBuilder.Entity<AirTemperature>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.AirTemperatures)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });

            modelBuilder.Entity<WaterLevel>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.WaterLevels)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();

                sn.HasOne(b => b.WaterLevelType)
                    .WithMany(s => s.WaterLevels)
                    .HasForeignKey(w => w.WaterLevelTypeId)
                    .IsRequired();
            });


            modelBuilder.Entity<User>(sn => {
                sn.Property(b => b.Name).HasColumnType("varchar(200)");
                sn.Property(b => b.Password).HasColumnType("varchar(500)");
            });

            modelBuilder.Entity<AirQuality>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.AirQualities)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });

            modelBuilder.Entity<GroundHumidity>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.GroundHumidities)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });


            modelBuilder.Entity<LUX>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.LuxMeasurements)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });


            modelBuilder.Entity<PH>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.PHLevels)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });


            modelBuilder.Entity<TDS>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.TdsMeasurements)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });


            modelBuilder.Entity<UVLight>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.UVLightLevels)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });

            modelBuilder.Entity<WaterTemperature>(sn => {
                sn.HasOne(b => b.Sensor)
                    .WithMany(s => s.WaterTemperatures)
                    .HasForeignKey(b => b.SensorId)
                    .IsRequired();
            });

            modelBuilder.Entity<GardenTask>(at => {
                at.HasOne(a => a.GardenTaskType)
                    .WithMany(aType => aType.GardenTask)
                    .HasForeignKey(b => b.GardenTaskTypeId)
                    .IsRequired();

                at.HasOne(a => a.Schedule)
                    .WithMany(s => s.Actions)
                    .HasForeignKey(b => b.ScheduleId)
                    .IsRequired();
            });

            modelBuilder.Entity<Schedule>(s => {
                s.HasOne(a => a.ScheduleType)
                    .WithMany(s => s.Schedules)
                    .HasForeignKey(b => b.ScheduleTypeId)
                    .IsRequired();
            });
        }
    }
}