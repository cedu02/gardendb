using System.Collections.Generic;

namespace GardenDB.Database.Models {
    public class GardenTaskType {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public int Code { get; set; }
        public List<GardenTask> GardenTask { get; set; } = new();
    }
}