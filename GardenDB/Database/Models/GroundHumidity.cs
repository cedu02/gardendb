using System;

namespace GardenDB.Database.Models {
    public class GroundHumidity {
        public long Id { get; set; }

        public DateTime Timestamp { get; set; }

        public float Humidity { get; set; }

        public int SensorId { get; set; }

        public Sensor Sensor { get; set; } = new();

        public int PlantId { get; set; }
        public Plant Plant { get; set; } = new();
    }
}