using System;

namespace GardenDB.Database.Models {
    public class GardenTask {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public int GardenTaskTypeId { get; set; }
        public int ScheduleId { get; set; }
        public GardenTaskType GardenTaskType { get; set; } = new();
        public Schedule Schedule { get; set; } = new();
    }
}