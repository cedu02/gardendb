using System;
using System.Collections.Generic;

namespace GardenDB.Database.Models {
    public class Schedule {
        public int Id { get; set; }
        public DateTime ActiveFrom { get; set; }
        public DateTime ActiveTo { get; set; }
        public float Interval { get; set; }

        public float Duration { get; set; }

        public int ScheduleTypeId { get; set; }

        public ScheduleType ScheduleType { get; set; } = new();
        public List<GardenTask> Actions { get; set; } = new();
    }
}