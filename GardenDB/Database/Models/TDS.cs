using System;

namespace GardenDB.Database.Models {
    public class TDS {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public float TdsMeasurements { get; set; }
        public float EC { get; set; }
        public int SensorId { get; set; }
        public Sensor Sensor { get; set; } = new();
    }
}