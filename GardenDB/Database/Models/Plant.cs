using System;
using System.Collections.Generic;

namespace GardenDB.Database.Models {
    public class Plant {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public DateTime PlantDate { get; set; }
        public string Species { get; set; } = string.Empty;
        public string BuyURl { get; set; } = string.Empty;
        public List<GroundHumidity> Humidities { get; set; } = new();
    }
}