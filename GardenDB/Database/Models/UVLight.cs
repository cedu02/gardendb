using System;

namespace GardenDB.Database.Models {
    public class UVLight {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public float UVLightLevel { get; set; }
        public int SensorId { get; set; }
        public Sensor Sensor { get; set; } = new();
    }
}