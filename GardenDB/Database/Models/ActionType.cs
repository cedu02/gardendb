using System.Collections.Generic;

namespace GardenDB.Database.Models {
    public class ActionType {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public int Code { get; set; }
        public List<Action> Actions { get; set; } = new();
    }
}