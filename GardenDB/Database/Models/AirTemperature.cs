using System;

namespace GardenDB.Database.Models {
    public class AirTemperature {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public float Kelvin { get; set; }

        public float Humidity { get; set; }

        public int SensorId { get; set; }
        public Sensor Sensor { get; set; } = new();
    }
}