using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GardenDB.Cache.Helper {
    public static class ByteArrayHelper {
        public static byte[] ObjectToByteArray(object obj) {
            if (obj == null) return new byte[0];

            BinaryFormatter bf = new();
            using var ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        public static T FromByteArray<T>(byte[] data) where T : new() {
            if (data == null)
                return new T();

            BinaryFormatter bf = new();
            using MemoryStream ms = new(data);
            object obj = bf.Deserialize(ms);
            return (T) obj;
        }
    }
}