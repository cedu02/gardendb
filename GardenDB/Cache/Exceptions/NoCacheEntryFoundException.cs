using System;

namespace GardenDB.Cache.Exceptions {
    public class NoCacheEntryFoundException : Exception {
    }
}