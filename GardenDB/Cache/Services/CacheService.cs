using System;
using System.Threading.Tasks;
using GardenDB.Cache.Exceptions;
using GardenDB.Cache.Interfaces;
using Microsoft.Extensions.Caching.Distributed;

namespace GardenDB.Cache.Services {
    public class CacheService : ICacheService {
        private readonly IDistributedCache _cache;

        public CacheService(IDistributedCache cache) {
            _cache = cache;
        }

        public async Task<bool> Register(string key, bool value) {
            return await Register(key, new[] {
                Convert.ToByte(value)
            });
        }

        public async Task<bool> Register(string key, byte[] value) {
            var options = new DistributedCacheEntryOptions();
            await _cache.SetAsync(key, value, options);
            return true;
        }

        public async Task<bool> Read(string key) {
            var result = await _cache.GetAsync(key);
            if (result != null) return BitConverter.ToBoolean(result);
            throw new NoCacheEntryFoundException();
        }

        public async Task<byte[]> ReadObject(string key) {
            var result = await _cache.GetAsync(key);
            if (result != null) return result;
            throw new NoCacheEntryFoundException();
        }

        public async Task<bool> Remove(string key) {
            await _cache.RemoveAsync(key);
            return true;
        }
    }
}