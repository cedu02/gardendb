using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Services.Cache;
using GardenDB.Cache.Helper;
using GardenDB.Cache.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Cache.Services {
    public class SensorCacheService : ISensorCacheService {
        private readonly ICacheService _cache;
        private readonly ILogger<SensorCacheService> _logger;

        public SensorCacheService(ICacheService cache, ILogger<SensorCacheService> logger) {
            _cache = cache;
            _logger = logger;
        }

        /// <summary>
        ///     Read sensor data from Cache.
        /// </summary>
        /// <param name="sensorEnum">Sensor identifier</param>
        /// <typeparam name="T">Type of value class</typeparam>
        /// <returns></returns>
        public async Task<T> GetValue<T>(SensorEnum sensorEnum) where T : new() {
            T result;
            try {
                var value = await _cache.ReadObject(sensorEnum.ToString());
                result = ByteArrayHelper.FromByteArray<T>(value);
            }
            catch (Exception e) {
                _logger.LogError("Couldn't read sensor data", sensorEnum, e);
                return new T();
            }

            return result;
        }

        /// <summary>
        ///     Save Sensor Data to Cache.
        /// </summary>
        /// <param name="sensorEnum">Sensor identifier</param>
        /// <param name="value">Values to save</param>
        /// <typeparam name="T">Type of value class</typeparam>
        /// <returns></returns>
        public async Task<bool> SetValue<T>(SensorEnum sensorEnum, T value) {
            if (value == null) return false;

            try {
                var result = await _cache.Register(sensorEnum.ToString(), ByteArrayHelper.ObjectToByteArray(value));
                _logger.LogDebug("Result of writing Cache {result}", result);
                return result;
            }
            catch (Exception e) {
                _logger.LogError("Couldn't set sensor date", sensorEnum, e);
                return false;
            }
        }
    }
}