using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Models;
using GardenDB.Cache.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenDB.Cache.Services {
    public class TaskCollector : ITaskCollector {
        private readonly ICacheService _cacheService;
        private readonly ILogger<TaskCollector> _logger;

        public TaskCollector(ICacheService cacheService, ILogger<TaskCollector> logger) {
            _cacheService = cacheService;
            _logger = logger; 
        }

        /// <summary>
        ///     Read Cache if certain task is open.
        /// </summary>
        /// <param name="gardenTask">Key of task to check</param>
        /// <returns></returns>
        public async Task<TaskModel> GetSwitchTask(GardenTaskEnum gardenTask) {
            try {
                return new TaskModel {
                    TaskFound = true,
                    StatusToChangeTo = await _cacheService.Read(gardenTask.Name)
                };
            }
            catch (Exception ex) {
                _logger.LogWarning(ex, "No task found on redis");
                return new TaskModel {
                    TaskFound = false
                };
            }
        }

        public async Task<bool> SetNewState(GardenTaskEnum gardenTask, bool state) {
            try {
                return await _cacheService.Register(gardenTask.ScheduleType.StateCheckAction.Value, state);
            }
            catch {
                return false;
            }
        }

        public async Task<bool> RemoveSwitchTask(GardenTaskEnum gardenTask) {
            return await _cacheService.Remove(gardenTask.Name);
        }
    }
}