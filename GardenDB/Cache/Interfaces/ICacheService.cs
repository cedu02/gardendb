using System.Threading.Tasks;

namespace GardenDB.Cache.Interfaces {
    public interface ICacheService {
        /// <summary>
        ///     Add or update key/value to cache.
        /// </summary>
        /// <param name="key">Key of cache entry.</param>
        /// <param name="value">Value for cache entry.</param>
        /// <returns></returns>
        public Task<bool> Register(string key, byte[] value);

        /// <summary>
        ///     Add or update bool value in cache.
        /// </summary>
        /// <param name="key">Key for cache entry</param>
        /// <param name="value">Bool value.</param>
        /// <returns></returns>
        public Task<bool> Register(string key, bool value);

        /// <summary>
        ///     Read bool value for key from cache.
        /// </summary>
        /// <param name="key">Key of cache value to read.</param>
        /// <returns></returns>
        public Task<bool> Read(string key);


        /// <summary>
        ///     Read object value for key from cache.
        /// </summary>
        /// <param name="key">Key of cache value to read.</param>
        /// <returns></returns>
        public Task<byte[]> ReadObject(string key);

        /// <summary>
        ///     Delete key/value pair form cache.
        /// </summary>
        /// <param name="key">Key to delete.</param>
        /// <returns></returns>
        public Task<bool> Remove(string key);
    }
}