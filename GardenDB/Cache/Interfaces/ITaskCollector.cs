using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Models;

namespace GardenDB.Cache.Interfaces {
    public interface ITaskCollector {
        /// <summary>
        ///     Read current value of switch task form cache.
        /// </summary>
        /// <param name="gardenTask">garden task</param>
        /// <returns></returns>
        public Task<TaskModel> GetSwitchTask(GardenTaskEnum gardenTask);

        /// <summary>
        ///     Set state of garden task item.
        /// </summary>
        /// <param name="gardenTask"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public Task<bool> SetNewState(GardenTaskEnum gardenTask, bool state);

        /// <summary>
        ///     Remove task from cache.
        /// </summary>
        /// <param name="gardenTask">garden task</param>
        /// <returns></returns>
        public Task<bool> RemoveSwitchTask(GardenTaskEnum gardenTask);
    }
}